import org.jetbrains.annotations.NotNull;
import res.*;

import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.io.OutputStream;
public class Oscilloscope {

    public static final void main(@NotNull String[] args) {
        String title = "ociloscope";
        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        lib.dimension=screenSize;
        JFrame window = new JFrame();
        window.setTitle(title);
        window.setBounds(0, 0, screenSize.width, screenSize.height);
        window.setLayout(new BorderLayout());
        window.setResizable(true);
        window.setDefaultCloseOperation(3);
        Graph graph = new Graph("Chenel 1", "Time", "Value");
        graph.setMAX(1000);
        final Serial_connector serial_connector = new Serial_connector(graph);
        final JEditorPane start = new JEditorPane();
        final JEditorPane end = new JEditorPane();
        CButton reset = new CButton("reset");
        CButton OK = new CButton("Ok");
        CButton skip = new CButton("skip");
        end.setBackground(Color.BLUE);
        start.setBackground(Color.GREEN);
        OK.addActionListener(it -> {
            lib.dataSize = Integer.parseInt(end.getText()) - Integer.parseInt(start.getText());
            String out = String.valueOf(Integer.parseInt(end.getText()) - Integer.parseInt(start.getText()));
            OutputStream outputStream = serial_connector.getChosenPort().getOutputStream();
            if(outputStream != null) {

                try {
                    outputStream.write(out.getBytes());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            System.out.println(lib.dataSize);
        });
        reset.addActionListener(e -> lib.dataSize=300);
        skip.addActionListener(it -> {
            String skip1 = end.getText();
            OutputStream outputStream = serial_connector.getChosenPort().getOutputStream();
            if(outputStream != null) {
                try {
                    outputStream.write(skip1.getBytes());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

        });

        /*
        dknob
         */
        DKnob ts;
        JLabel jl;
        ChangeListener cl;
        CButton cButton=new CButton("Done");
        JPanel panel =new JPanel();
        panel.add(ts = new DKnob(), BorderLayout.CENTER);
        panel.add(jl = new JLabel("Volume: 0"), BorderLayout.NORTH);
        panel.add(cButton);
        ts.setValue((float)1.0);
        final JLabel jla = jl;
        ts.addChangeListener(new ChangeListener() {
            int priv=300;
            public void stateChanged(ChangeEvent e) {
                DKnob t = (DKnob) e.getSource();
                int vol;
                jla.setText("Volume: " + (vol = (int)(100*t.getValue())));
                lib.dataSize=vol;
            }
        });
        cButton.addActionListener(e -> {
            String out = String.valueOf(lib.dataSize);
            OutputStream outputStream = serial_connector.getChosenPort().getOutputStream();
            if(outputStream != null) {

                try {
                    outputStream.write(out.getBytes());
                } catch (IOException e1) {
                    e1.printStackTrace();
                }
            }
        });

        CJpanel btnPanel = new CJpanel();
        btnPanel.add(start);
        btnPanel.add(end);
        btnPanel.add(OK);
        btnPanel.add(reset);
        btnPanel.add(skip);
        btnPanel.add(panel);
        window.add(serial_connector.getPanel(), "North");
        window.add(btnPanel, "South");
        window.add(graph.getPanel());
        window.setVisible(true);
    }
}
