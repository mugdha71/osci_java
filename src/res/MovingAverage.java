package res;

public final class MovingAverage {
    private final double[] window;
    private double sum;
    private int fill;
    private int position;

    public final void add(double number) {
        if (fill == window.length) {
            sum -= window[position];
        } else {
            fill++;
        }

        sum += number;
        window[position++] = number;

        if (position == window.length) {
            position = 0;
        }

    }

    public final double getAverage() {
        return this.sum / (double)this.fill;
    }

    public MovingAverage(int size) {
        this.window = new double[size];
    }
}
