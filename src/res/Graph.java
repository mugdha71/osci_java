package res;

import org.jetbrains.annotations.Nullable;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.renderer.xy.XYItemRenderer;
import org.jfree.chart.renderer.xy.XYLineAndShapeRenderer;
import org.jfree.chart.renderer.xy.XYSplineRenderer;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;

import java.awt.*;
import java.awt.geom.Ellipse2D.Double;
import java.util.Iterator;
import java.util.List;

public final class Graph {
    private CJpanel panel;
    private XYSeries series;
    private XYSeriesCollection dataset;
    private JFreeChart chart;
    private XY_data lastData;
    private int MAX;
    private boolean SHOW_LINE;
    private boolean SHOW_SHAPE;
    private boolean SHOW_LEDGEND;

    
    public final CJpanel getPanel() {
        return this.panel;
    }

    public final void setPanel( CJpanel panel) {
        this.panel = panel;
    }

    
    public final XYSeries getSeries() {
        return this.series;
    }

    public final void setSeries( XYSeries series) {
        this.series = series;
    }

    
    public final XYSeriesCollection getDataset() {
        return this.dataset;
    }

    public final void setDataset( XYSeriesCollection xySeriesCollection) {
        this.dataset = xySeriesCollection;
    }

    
    public final JFreeChart getChart() {
        return this.chart;
    }

    public final void setChart( JFreeChart jFreeChart) {
        this.chart = jFreeChart;
    }

    @Nullable
    public final XY_data getLastData() {
        return this.lastData;
    }

    public final void setLastData(@Nullable XY_data xy_data) {
        this.lastData = xy_data;
    }

    public final int getMAX() {
        return this.MAX;
    }

    
    public final boolean getSHOW_LINE() {
        return this.SHOW_LINE;
    }

    public final void setSHOW_LINE(boolean flag_Show_line) {
        this.SHOW_LINE = flag_Show_line;
    }

    public final boolean getSHOW_SHAPE() {
        return this.SHOW_SHAPE;
    }

    public final void setSHOW_SHAPE(boolean flag_Show_shape) {
        this.SHOW_SHAPE = flag_Show_shape;
    }

    public final boolean getSHOW_LEDGEND() {
        return this.SHOW_LEDGEND;
    }


    public final void setSHOW_LEDGEND(boolean flag_Show_ledgend) {
        this.SHOW_LEDGEND = flag_Show_ledgend;
    }

    public final void setSerise( List data) {
        this.series.clear();
        Iterator iterator = data.iterator();

        while(iterator.hasNext()) {
            XY_data d = (XY_data)iterator.next();
            this.series.add(d.getX(), d.getY());
        }

        this.repaint();
    }

    public final void new_data( XY_data xy) {
        this.series.add(xy.getX(), xy.getY());
        this.repaint();
    }

    public final void new_data(double x, double y) {
        this.series.add(x, y);
        this.repaint();
    }

    public final void dataListAdd( List data) {
        this.dataset.removeAllSeries();
        XYSeries series = new XYSeries((Comparable)"Value 1");
        Iterator var4 = data.iterator();

        while(var4.hasNext()) {
            XY_data xy_data = (XY_data)var4.next();
            series.add(xy_data.getX(), xy_data.getY());
        }

        this.dataset.addSeries(series);
        this.panel.repaint();
    }

    public final void nullData(double x) {
        this.series.add(x, (Number)null);
        this.repaint();
    }

    public final void repaint() {
        if(this.MAX != -1) {
            List series = this.dataset.getSeries();
            List allSerieses = series;
            Iterator iterator = allSerieses.iterator();

            while(iterator.hasNext()) {
                Object s = iterator.next();
                if(((XYSeries)s).getItemCount() > this.MAX) {
                    ((XYSeries)s).remove(0);
                }
            }
        }

        int n = this.dataset.getSeriesCount();
        int i = 0;
        if(i <= n-1) {
            while(true) {
                XYItemRenderer xyItemRenderer = this.chart.getXYPlot().getRenderer();

                XYLineAndShapeRenderer renderer = (XYLineAndShapeRenderer)xyItemRenderer;
                renderer.setSeriesVisibleInLegend(i, Boolean.valueOf(this.SHOW_LEDGEND));
                renderer.setSeriesLinesVisible(i, this.SHOW_LINE);
                renderer.setSeriesShapesVisible(i, this.SHOW_SHAPE);
                if(i == n-1) {
                    break;
                }

                ++i;
            }
        }

        this.panel.repaint();
    }

    public final void setMAX(int MAX) {
        this.MAX = MAX;
    }

    public final void clear() {
        this.dataset.removeAllSeries();
        this.series = new XYSeries((Comparable)"Line 1");
        this.dataset.addSeries(this.series);
    }

    public Graph( String titel,  String xAxis,  String yAxis) {
        super();
        this.panel = new CJpanel();
        this.series = new XYSeries((Comparable)"Line One");
        this.dataset = new XYSeriesCollection();
        this.MAX = -1;
        this.SHOW_LINE = true;
        this.SHOW_SHAPE = true;
        this.SHOW_LEDGEND = true;
        JFreeChart jFreeChart = ChartFactory.createXYLineChart(titel, xAxis, yAxis, this.dataset);
        this.chart = jFreeChart;
        this.dataset.addSeries(this.series);
        ChartPanel cp = new ChartPanel(this.chart);
        new XYSplineRenderer();
        XYLineAndShapeRenderer renderer = new XYLineAndShapeRenderer();
        renderer.setDrawOutlines(true);
        renderer.setUseOutlinePaint(true);
        renderer.setSeriesPaint(0, Color.BLUE);
        renderer.setSeriesOutlinePaint(0, Color.BLACK);
        Double ellipse2D = new Double(-1.0D, -1.0D, 5.0D, 5.0D);
        renderer.setSeriesShape(0, ellipse2D);
        renderer.setSeriesToolTipGenerator(0, (xyDataset, i, i1) -> "(Data position:"+i1+" X axis:"+xyDataset.getX(i,i1)+" Y axis:"+xyDataset.getY(i,i1)+")");
        this.chart.getXYPlot().getRangeAxis().setUpperBound(5.0D);
        this.chart.getXYPlot().getRangeAxis().setLowerBound(-5.0D);
        cp.setMinimumSize(new Dimension((int)lib.getDimension().getWidth(), (int)lib.getDimension().getHeight() - 200));
        cp.setPreferredSize(new Dimension((int)lib.getDimension().getWidth(), (int)lib.getDimension().getHeight() - 200));
        this.chart.getXYPlot().setRenderer(renderer);
        this.panel.add(cp);
        this.repaint();
    }
}
