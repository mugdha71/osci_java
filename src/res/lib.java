package res;

import java.awt.*;

/**
 * Created by Mugdha on 7/9/2017.
 */
public class lib {
    public static Color back = Color.decode("#FFFFFF");
    public static Color dark = Color.decode("#64b5f6");
    public static Color bright = Color.decode("#2196f3");
    public static Dimension dimension=new  Dimension(700,700);
    public static int dataSize=100;
    public static int skip=0;

    public static Color getBack() {
        return back;
    }

    public static void setBack(Color back) {
        lib.back = back;
    }

    public static Color getDark() {
        return dark;
    }

    public static void setDark(Color dark) {
        lib.dark = dark;
    }

    public static Color getBright() {
        return bright;
    }

    public static void setBright(Color bright) {
        lib.bright = bright;
    }

    public static Dimension getDimension() {
        return dimension;
    }

    public static void setDimension(Dimension dimension) {
        lib.dimension = dimension;
    }

    public static int getDataSize() {
        return dataSize;
    }

    public static void setDataSize(int dataSize) {
        lib.dataSize = dataSize;
    }

    public static int getSkip() {
        return skip;
    }

    public static void setSkip(int skip) {
        lib.skip = skip;
    }



}
