package res;

import com.fazecast.jSerialComm.SerialPort;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.swing.*;
import java.util.ArrayList;
import java.util.Scanner;

public final class Serial_connector {
    @NotNull
    private JComboBox portList_combobox;
    @NotNull
    private CButton connectButton;
    @NotNull
    private JCheckBox ac;
    @NotNull
    private JCheckBox dc;
    @NotNull
    private CJpanel panel;
    @NotNull
    private CButton pauseButton;
    @NotNull
    private ArrayList data;
    private boolean play;
    @Nullable
    private SerialPort chosenPort;
    @NotNull
    private JCheckBox inp1;
    @NotNull
    private JCheckBox inp2;

    @NotNull
    public final JComboBox getPortList_combobox() {
        return this.portList_combobox;
    }

    public final void setPortList_combobox(@NotNull JComboBox jComboBox) {
        this.portList_combobox = jComboBox;
    }

    @NotNull
    public final CButton getConnectButton() {
        return this.connectButton;
    }

    public final void setConnectButton(@NotNull CButton cButton) {
        this.connectButton = cButton;
    }

    @NotNull
    public final JCheckBox getAc() {
        return this.ac;
    }

    public final void setAc(@NotNull JCheckBox jCheckBox) {
        this.ac = jCheckBox;
    }

    @NotNull
    public final JCheckBox getDc() {
        return this.dc;
    }

    public final void setDc(@NotNull JCheckBox jCheckBox) {
        this.dc = jCheckBox;
    }

    @NotNull
    public final CJpanel getPanel() {
        return this.panel;
    }

    public final void setPanel(@NotNull CJpanel cJpanel) {
        this.panel = cJpanel;
    }

    @NotNull
    public final CButton getPauseButton() {
        return this.pauseButton;
    }

    public final void setPauseButton(@NotNull CButton cButton) {
        this.pauseButton = cButton;
    }

    @NotNull
    public final ArrayList getData() {
        return this.data;
    }

    public final void setData(@NotNull ArrayList arrayList) {
        this.data = arrayList;
    }

    public final boolean getPlay() {
        return this.play;
    }

    public final void setPlay(boolean flag_Play) {
        this.play = flag_Play;
    }

    @Nullable
    public final SerialPort getChosenPort() {
        return this.chosenPort;
    }

    public final void setChosenPort(@Nullable SerialPort serialPort) {
        this.chosenPort = serialPort;
    }

    @NotNull
    public final JCheckBox getInp1() {
        return this.inp1;
    }

    public final void setInp1(@NotNull JCheckBox jCheckBox) {
        this.inp1 = jCheckBox;
    }

    @NotNull
    public final JCheckBox getInp2() {
        return this.inp2;
    }

    public final void setInp2(@NotNull JCheckBox jCheckBox) {
        this.inp2 = jCheckBox;
    }


    void connect(Graph graph)
    {
        if(connectButton.getText().equalsIgnoreCase("Connect"))
        {
            chosenPort=SerialPort.getCommPort(portList_combobox.getSelectedItem().toString());
            chosenPort.setComPortParameters(250000, 8, 1, 0);
            chosenPort.setComPortTimeouts(SerialPort.TIMEOUT_SCANNER, 0, 0);
            if(chosenPort.openPort())
            {
                connectButton.setText("Disconnect");
                pauseButton.setVisible(true);
                portList_combobox.setEnabled(false);
            }
            else {
                System.out.println("Not connected");
                return;
            }
            Thread thread = new Thread() {
                public void run() {
                    Scanner scanner =new Scanner(chosenPort.getInputStream());
                    double x = 0.0D;
                    double y = 0.0D;
                    double st = (double)System.nanoTime();
                    double tempX = st;
                    double tempY = 1.0D;
                    graph.setMAX(1000);
                    MovingAverage ma = new MovingAverage(15);
                    double preX = 0.0D;
                    double x1 = 0.0D;
                    preX = (double)System.nanoTime();
                    String[] values;
                    while(scanner.hasNextLine()) {
                        try {
                            if(play) {
                                String line = scanner.nextLine();
                                values=line.split(",");
                                if(inp1.isSelected() && inp2.isSelected())
                                {
                                    y = java.lang.Double.valueOf(values[1]);
                                    x = java.lang.Double.valueOf(values[3]);//x=System.nanoTime()-st;
                                }
                                else if(inp1.isSelected())
                                {
                                    y = java.lang.Double.valueOf(values[1]);
                                    x += java.lang.Double.valueOf(values[0]);//x=System.nanoTime()-st;
                                }
                                else if(inp2.isSelected())
                                {
                                    y = java.lang.Double.valueOf(values[3]);
                                    x += java.lang.Double.valueOf(values[2]);//x=System.nanoTime()-st;
                                }
                                if (y >=2048.0) {//if (data.size >= lib.dataSize) {
                                    //graph.nullData(x)
                                    System.out.println(data.size());
                                    graph.dataListAdd(data);
                                    data.clear();
                                    //ma = new MovingAverage(5);
                                    x=0.0;
                                    x1= (double) System.nanoTime();
                                    double dif = x1 - preX;
                                    System.out.println("done at : "+dif);
                                    preX=x1;
                                } else if (Math.abs(x - tempX) > 0) {
                                    tempY = y;
                                    ma.add(y);
                                    //double d = ma.getAverage();

                                    if (ac.isSelected()){
                                        //graph.new_data(x, d)
                                        data.add(new XY_data(x,y));
                                    }

                                    else
                                    if(lib.skip<=0)
                                        data.add(new XY_data(x,y));
                                        //graph.new_data(x, y)
                                    else lib.skip--;
                                }
                                tempX = x;
                            }
                        } catch (Exception e) {
                        }
                    }

                    scanner.close();
                }
            };
            thread.start();
        }
        else {
            // disconnect from the serial port
            chosenPort.closePort();
            portList_combobox.setEnabled(true);
            pauseButton.setVisible(true);
            graph.clear();
            connectButton.setText("Connect");
        }

    }
    public Serial_connector(@NotNull final Graph graph) {
        super();
        this.portList_combobox = new JComboBox();
        this.connectButton = new CButton("Connect");
        this.ac = new JCheckBox("AC", true);
        this.dc = new JCheckBox("DC", false);
        this.panel = new CJpanel();
        this.pauseButton = new CButton("Start/Pause");
        this.data = new ArrayList();
        this.inp1 = new JCheckBox("INP 1", true);
        this.inp2 = new JCheckBox("INP 2", false);
        this.panel.add(this.portList_combobox);
        this.panel.add(this.connectButton);
        this.panel.add(this.pauseButton);
        this.panel.add(this.inp1);
        this.panel.add(this.inp2);
        this.ac.addActionListener(it -> dc.setSelected(!dc.isSelected()));
        this.dc.addActionListener(it -> ac.setSelected(!ac.isSelected()));
        this.pauseButton.setVisible(false);
        this.pauseButton.addActionListener(it -> {
            if(play) {
                play=false;
            } else {
                play=true;
            }

        });
        this.connectButton.addActionListener(it -> connect(graph));
        SerialPort[] portNames = SerialPort.getCommPorts();
        Thread thread_port = new Thread() {
            public void run() {
                while(true) {
                    SerialPort[] sp = SerialPort.getCommPorts();
                    if(sp.length > 0) {
                        for(int var3 = 0; var3 < sp.length; ++var3) {
                            SerialPort sp_name = sp[var3];
                            int l = portList_combobox.getItemCount();

                            int i;
                            for(i = 0; i < l && !sp_name.getSystemPortName().equals(portList_combobox.getItemAt(i)); ++i) {

                            }

                            if(i == l) {
                                portList_combobox.addItem(sp_name.getSystemPortName());
                            }
                        }
                    } else {
                        portList_combobox.removeAllItems();
                    }

                    portList_combobox.repaint();
                }
            }
        };
        thread_port.start();

        for(int i = 0; i < portNames.length; ++i) {
            SerialPort sp_name = portNames[i];
            portList_combobox.addItem(sp_name.getSystemPortName());
        }

    }
}
