package res;

public final class XY_data {
    private double x;
    private double y;
    private double xSd;
    private double ySd;

    public final double getX() {
        return this.x;
    }

    public final void setX(double var1) {
        this.x = var1;
    }

    public final double getY() {
        return this.y;
    }

    public final void setY(double var1) {
        this.y = var1;
    }

    public final double getXSd$production_sources_for_module_Oscilloscope() {
        return this.xSd;
    }

    public final void setXSd$production_sources_for_module_Oscilloscope(double var1) {
        this.xSd = var1;
    }

    public final double getYSd$production_sources_for_module_Oscilloscope() {
        return this.ySd;
    }

    public final void setYSd$production_sources_for_module_Oscilloscope(double var1) {
        this.ySd = var1;
    }

    public final double getxSd() {
        return this.xSd;
    }

    public final double getySd() {
        return this.ySd;
    }

    public final void setxSd(double xSd) {
        this.xSd = xSd;
    }

    public final void setySd(double ySd) {
        this.ySd = ySd;
    }

    public XY_data(double x, double xSd, double y, double ySd) {
        this.x = x;
        this.y = y;
        this.xSd = xSd;
        this.ySd = ySd;
    }

    public XY_data(double x, double y) {
        super();
        this.x = x;
        this.y = y;
        this.xSd = 0.0D;
        this.ySd = 0.0D;
    }
}
