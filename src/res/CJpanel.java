package res;

import org.jetbrains.annotations.NotNull;

import javax.swing.*;
import java.awt.*;
public final class CJpanel extends JPanel {
    public CJpanel(@NotNull LayoutManager layout, boolean isDoubleBuffered) {
        super(layout, isDoubleBuffered);
        this.setBackground(lib.getBack());
    }

    public CJpanel(@NotNull LayoutManager layout) {
        super(layout);
        this.setBackground(lib.getBack());
    }

    public CJpanel(boolean isDoubleBuffered) {
        super(isDoubleBuffered);
        this.setBackground(lib.getBack());
    }

    public CJpanel() {
        this.setBackground(lib.getBack());
    }
}
