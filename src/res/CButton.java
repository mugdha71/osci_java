package res;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import javax.swing.Icon;
import javax.swing.JButton;
import org.jetbrains.annotations.NotNull;

public final class CButton extends JButton implements MouseListener {
    private int edgW;
    private int edgH;

    public final int getEdgW() {
        return this.edgW;
    }

    public final void setEdgW(int edgW) {
        this.edgW = edgW;
    }

    public final int getEdgH() {
        return this.edgH;
    }

    public final void setEdgH(int edgH) {
        this.edgH = edgH;
    }

    protected void paintComponent(@NotNull Graphics g) {
        if(this.getModel().isArmed()) {
            g.setColor(this.getBackground());
        } else {
            g.setColor(this.getBackground());
        }

        g.fillRoundRect(0, 0, this.getWidth() - 1, this.getHeight() - 1, this.edgW, this.edgH);
        super.paintComponent(g);
    }

    protected void paintBorder(@NotNull Graphics g) {
        g.setColor(this.getBackground());
        g.drawRoundRect(0, 0, this.getWidth() - 1, this.getHeight() - 1, this.edgW, this.edgH);
    }

    public void mouseClicked(@NotNull MouseEvent e) {

    }

    public void mousePressed(@NotNull MouseEvent e) {

    }

    public void mouseReleased(@NotNull MouseEvent e) {

    }

    public void mouseEntered(@NotNull MouseEvent e) {
        super.setBackground(lib.getBright());
    }

    public void mouseExited(@NotNull MouseEvent e) {
        super.setBackground(lib.getDark());
    }

    public CButton(@NotNull String text) {
        super(text);
        this.edgW = 7;
        this.edgH = 7;
        this.setContentAreaFilled(false);
        super.setBackground(lib.getDark());
        super.setForeground(Color.white);
        super.addMouseListener(this);
        super.setBorderPainted(false);
        super.setFocusPainted(false);
    }

    public CButton(@NotNull Icon icon) {
        super(icon);
        this.edgW = 7;
        this.edgH = 7;
        super.setBackground(lib.getDark());
        super.setForeground(Color.white);
        super.addMouseListener(this);
        super.setBorderPainted(false);
        super.setBorderPainted(false);
        super.setFocusPainted(false);
    }
}
